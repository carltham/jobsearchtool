/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.jobborse.domain.users;

/**
 *
 * @author carl
 */
public class UserBuilder {

    private User user;

    public UserBuilder() {
    }

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public UserBuilder withEmail(String newEmail) {
        getUser().setEmail(newEmail);
        return this;
    }

    public UserBuilder withPassword(String newPassword) {
        getUser().setPassword(newPassword);
        return this;
    }

    public User build() {
        return getUser();
    }

}
