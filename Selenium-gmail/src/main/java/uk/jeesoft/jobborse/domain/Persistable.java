/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.jobborse.domain;

/**
 *
 * @author carl
 */
public interface Persistable {

    public String getId();

    public String getName();

}
