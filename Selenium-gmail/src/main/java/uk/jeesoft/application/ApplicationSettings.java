/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.application;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;
import java.util.TreeMap;
import uk.jeesoft.jobborse.domain.users.User;
import uk.jeesoft.jobborse.domain.users.UserBuilder;
import uk.jeesoft.selenium.utils.Search;

/**
 *
 * @author carl.sundvisson
 */
public class ApplicationSettings {

    public static String _GMAIL_ID = "myGmailId_";
    public static String _GMAIL_PASSWORD = "myGmailPassword";

    public static boolean _FOLLOW_EXTERNAL_REDIRECT = false;
    public static String _EXTERNAL_REDIRECT_DEFINITION = "/redirect_extern?url=";

    public static final String jobborseURL = "https://jobboerse.arbeitsagentur.de/vamJB/startseite.html?aa=1&m=1&kgr=as&vorschlagsfunktionaktiv=true";

    public static Search[] _SEARCHES_AREA_FRANKFURT_JAVA = new Search[]{
        new Search(jobborseURL, "Arbeit", "java", "Frankfurt am Main", true),
        new Search(jobborseURL, "Arbeit", "java", "Mainz am Rhein", true),
        new Search(jobborseURL, "Arbeit", "java", "Wiesbaden", true),
        new Search(jobborseURL, "Arbeit", "java", "Darmstadt", true),
        new Search(jobborseURL, "Arbeit", "java", "Hanau", true)
    };

    public static Search[] _SEARCHES_ONLY_FRANKFURT_JAVA = new Search[]{
        new Search(jobborseURL, "Arbeit", "java", "Frankfurt am Main", true)
    };

    public static Search[] _SEARCHES_ONLY_MUNICH_JAVA = new Search[]{
        new Search(jobborseURL, "Arbeit", "java", "München", true)
    };

    public static Search[] _SEARCHES_AREA_FRANKFURT_INTERN_JAVA = new Search[]{
        new Search(jobborseURL, "Praktikum/ Trainee", "java", "Frankfurt am Main", true),
        new Search(jobborseURL, "Praktikum/ Trainee", "java", "Mainz am Rhein", true),
        new Search(jobborseURL, "Praktikum/ Trainee", "java", "Wiesbaden", true),
        new Search(jobborseURL, "Praktikum/ Trainee", "java", "Darmstadt", true),
        new Search(jobborseURL, "Praktikum/ Trainee", "java", "Hanau", true)
    };

    public static final TreeMap<String, Search> _SEARCHES_AREA = new TreeMap<String, Search>();

    public static int _STANDARD_PAGE_WIDTH_IN_PIXEL = 1280;
    private static boolean isDevMode = false;
    public static int _ALL = -1;
    private static String _WEBDRIVER_CHROME = "chromedriver";
    private static String _WEBDRIVER_FIREFOX = "geckodriver";
    public static String _SELECTED_WEBDRIVER = _WEBDRIVER_FIREFOX;
    private static String _WEBDRIVER_CHROME_SYSTEM_PROPERTY = "webdriver.chrome.driver";
    private static String _WEBDRIVER_FIREFOX_SYSTEM_PROPERTY = "webdriver.gecko.driver";
    public static String _SELECTED_WEBDRIVER_SYSTEM_PROPERTY = _WEBDRIVER_FIREFOX_SYSTEM_PROPERTY;

    public static void setDevMode(boolean devMode) {
        isDevMode = devMode;
    }

    public static boolean isIsDevMode() {
        return isDevMode;
    }

    public static User loadUser() throws IOException {
        File userFile = getUserFile();
        FileReader userFilereader = new FileReader(userFile);
        Properties userProperties = new Properties();
        userProperties.load(userFilereader);
        User user = new UserBuilder().
                withEmail((String) userProperties.getProperty("email")).
                withPassword((String) userProperties.getProperty("password"))
                .build();

        if (user.getEmail() == null || user.getPassword() == null) {
            user.setEmail(ApplicationSettings._GMAIL_ID);
            user.setPassword(ApplicationSettings._GMAIL_PASSWORD);
            ApplicationSettings.saveUser(user);
        }
        return user;
    }

    public static File getUserFile() throws IOException {
        File userFile = new File(System.getProperty("user.home")
                + "/gmail.properties");
        if (!userFile.exists()) {
            userFile.getParentFile().mkdirs();
            userFile.createNewFile();
        }
        return userFile;
    }

    public static void saveUser(User user) throws IOException {
        Properties userProperties = new Properties();
        userProperties.setProperty("email", user.getEmail());
        userProperties.setProperty("password", user.getPassword());
        File userFile = new File(System.getProperty("user.home")
                + "/gmail.properties");
        Writer writer = new FileWriter(userFile);
        userProperties.store(writer, "These credentials are not valid if not changed!");
    }
}
