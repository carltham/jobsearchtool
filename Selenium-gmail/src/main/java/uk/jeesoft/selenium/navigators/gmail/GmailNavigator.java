/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.navigators.gmail;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.jeesoft.exceptions.LoginFailiureException;
import uk.jeesoft.jobborse.domain.users.User;
import uk.jeesoft.selenium.navigators.GmailChromeNavigator;

/**
 *
 * @author jaya
 */
public class GmailNavigator {

    private static GmailChromeNavigator navigatiorInstance;

    public static void quit() throws IOException, InterruptedException {
        getInstance().quit();
    }

    public static String getTitle() throws IOException, InterruptedException {
        return getInstance().getTitle();
    }

    public static void doLogin(User user) throws IOException, InterruptedException, LoginFailiureException {
        getInstance().doLogin(user.getEmail(), user.getPassword());
    }

    public static WebElement retryingFindClick(By by) throws IOException, InterruptedException {
        return getInstance().retryingFindClick(by);
    }

    private static GmailChromeNavigator getInstance() throws IOException, InterruptedException {
        if (navigatiorInstance == null) {
            navigatiorInstance = new GmailChromeNavigator();
        }
        return navigatiorInstance;
    }

    public static void setDriver(WebDriver newDriver) throws IOException, InterruptedException {
        getInstance().setDriver(newDriver);
    }
}
