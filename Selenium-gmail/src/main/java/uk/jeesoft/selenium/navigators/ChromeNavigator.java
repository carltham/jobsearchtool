/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.navigators;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author carl
 */
public class ChromeNavigator {

    private WebDriver _driver;

    public final void quit() throws IOException, InterruptedException {
//        getDriver().close();
        getDriver().quit();
    }

    public final WebDriver getDriver() {
        return _driver;
    }

    public final void setDriver(WebDriver newDriver) {
        this._driver = newDriver;
    }

    public final String getTitle() throws IOException, InterruptedException {
        return getDriver().getTitle();
    }

    public final void fillBy(By by, String value) throws IOException, InterruptedException {
        WebElement field = retryingFindClick(by);
        try {
            field.clear();
        } catch (InvalidElementStateException exception) {
            System.out.println("Exception : " + exception.getMessage());
        }
        field.sendKeys(value);
    }

    public final WebElement retryingFindClick(By by) throws IOException, InterruptedException {
        WebElement foundElement = null;
        int attempts = 0;
        while (++attempts < 5 && foundElement == null) {
            try {

                foundElement = getDriver().findElement(by);
                WebDriverWait wait = new WebDriverWait(getDriver(), 2000);

                wait.until(ExpectedConditions.visibilityOf(foundElement));

                Actions action = new Actions(getDriver()).moveToElement(foundElement);
//                action.click().perform();
                action.perform();
//                break;
            } catch (StaleElementReferenceException e) {
                System.out.println(e.getMessage());
//                Thread.sleep(200);
            } catch (NoSuchElementException e) {
                System.out.println(e.getMessage());
//                Thread.sleep(200);
                throw e;
            }
            foundElement.click();
        }

//        System.out.println("Triedto click " + by + " :: " + attempts + " times");
        return foundElement;
    }

}
