/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.navigators;

import java.io.IOException;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import uk.jeesoft.application.ApplicationSettings;
import uk.jeesoft.exceptions.LoginFailiureException;

/**
 *
 * @author carl
 */
public class GmailChromeNavigator extends ChromeNavigator {

    private ArrayList<String> tabs2;
    private String presentTab;
    private int pageNr = 1;

    public GmailChromeNavigator() throws IOException, InterruptedException {

    }

    public void doLogin(String id, String password) throws IOException, InterruptedException, LoginFailiureException {
        fillBy(By.id("identifierId"), id);

        moveNext();

        By by;
        try {
            by = By.xpath("//div[contains(@jsaction,\"clickonly:\")]/div[.=\"Couldn't find your Google Account\"]");
            WebElement element = retryingFindClick(by);
            element.click();
            System.out.println("****************************************************************************************************");
            String msg = "Login failiure, update username in file :" + ApplicationSettings.getUserFile().getAbsolutePath();
            System.out.println(msg);
            System.out.println("****************************************************************************************************");
            throw new LoginFailiureException(msg);
        } catch (WebDriverException e) {
            System.out.println("****************************************************************************************************");
            System.out.println("WebDriverException message : " + e.getMessage());
            System.out.println("Username correct");
            System.out.println("****************************************************************************************************");
        }

        fillBy(By.name("password"), password);

        moveNext();
//        Thread.sleep(100);
        by = By.xpath("//input[contains(@aria-label, 'Search')]");
        retryingFindClick(by);
    }

    public void moveNext() throws InterruptedException, IOException {
        By by = By.xpath("//span[.='Next']");
        retryingFindClick(by);
    }
}
