/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.application;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

/**
 *
 * @author carl
 */
public class ApplicationSettingsTest {

    /**
     * Test of setDevMode method, of class ApplicationSettings.
     */
    @Test
    public void shouldSetDevmode_true() {
        System.out.println("setDevMode");
        boolean devMode = true;
        ApplicationSettings.setDevMode(devMode);
        assertThat(ApplicationSettings.isIsDevMode()).isTrue();
    }

    /**
     * Test of isIsDevMode method, of class ApplicationSettings.
     */
    @Test
    public void devMode_should_be_fale_by_default() {
        System.out.println("isIsDevMode");
        boolean expResult = false;
        boolean result = ApplicationSettings.isIsDevMode();
        assertThat(expResult).isEqualTo(result);
    }

}
