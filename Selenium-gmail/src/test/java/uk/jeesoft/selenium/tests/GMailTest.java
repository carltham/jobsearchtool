package uk.jeesoft.selenium.tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.jeesoft.application.ApplicationSettings;
import uk.jeesoft.exceptions.LoginFailiureException;
import uk.jeesoft.log.GlobalLog;
import uk.jeesoft.selenium.Init;
import uk.jeesoft.selenium.navigators.gmail.GmailNavigator;

public class GMailTest extends AbstractSeleniumTest {

    private static String browserURL = "https://accounts.google.com/signin/v2/identifier"
            + "?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail"
            + "&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin";
    WebElement element;
    private static boolean isLoggedin;

    @BeforeClass
    public static void openBrowser() throws IOException, InterruptedException {
        Init.setBrowserURL(browserURL);
        Init.initDriver();
        WebDriver tmpDriver = Init.createDriver();
        GmailNavigator.setDriver(tmpDriver);
    }

    @AfterClass
    public static void closeBrowser() throws IOException, InterruptedException {
        GmailNavigator.quit();
    }

    @Test
    public void findTitle() throws IOException, InterruptedException {

        checkTitle();
    }

    @Test
    public void loginToGmail_when_gmail_properties_exists_and_correct() throws IOException, InterruptedException, LoginFailiureException {

        GlobalLog.logEnter(new Object() {
        });
        GmailNavigator.doLogin(ApplicationSettings.loadUser());
        isLoggedin = true;

        GlobalLog.logExit(new Object() {
        });
    }

    @Test(expected = LoginFailiureException.class)
    public void login_must_fail_when_gmail_properties_does_not_exists() throws IOException, InterruptedException, LoginFailiureException {

        GlobalLog.logEnter(new Object() {
        });
        renameUserFile(true);
        try {
            GmailNavigator.doLogin(ApplicationSettings.loadUser());
        } catch (LoginFailiureException e) {
            isLoggedin = true;
            renameUserFile(false);
            throw e;
        }
        fail("Should never reach here");
        GlobalLog.logExit(new Object() {
        });
    }

    @Test
    public void logoutFromGmail() throws IOException, InterruptedException, LoginFailiureException {

        GlobalLog.logEnter(new Object() {
        });

        if (!isLoggedin) {
            GmailNavigator.doLogin(ApplicationSettings.loadUser());
        }

        By by = By.xpath("//a[starts-with(@href, 'https://accounts.google.com/SignOutOptions?')]");
        GmailNavigator.retryingFindClick(by);

        by = By.xpath("//a[.='Sign out']");
        GmailNavigator.retryingFindClick(by);

        GlobalLog.logExit(new Object() {
        });
    }

    private void checkTitle() throws IOException, InterruptedException {
        String googleTitle = GmailNavigator.getTitle();
        System.out.println("Application title is :: " + googleTitle);
        String expectedTitle = "Gmail";
        assertThat(googleTitle).contains(expectedTitle);
    }

    private void renameUserFile(boolean makeABackup) throws IOException {

        if (makeABackup) {
            Path source = Paths.get(System.getProperty("user.home")
                    + "/gmail.properties");
            Files.move(source, source.resolveSibling(System.getProperty("user.home")
                    + "/gmail.properties.bak"));
        } else {
            File fileToRemove = new File(System.getProperty("user.home")
                    + "/gmail.properties");
            fileToRemove.delete();

            Path source = Paths.get(System.getProperty("user.home")
                    + "/gmail.properties.bak");
            Files.move(source, source.resolveSibling(System.getProperty("user.home")
                    + "/gmail.properties"));
        }
    }
}
