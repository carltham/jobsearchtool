/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.log;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.jeesoft.application.ApplicationSettings;

/**
 *
 * @author carl.sundvisson
 */
public class GlobalLog {

    private static GlobalLog instance = new GlobalLog();
    private static Level level = Level.WARNING;
    private static Logger _logger = Logger.getGlobal();

    public static void logEnter(Object enclosingFunctionObject) throws SecurityException {
        System.out.println();
        System.out.println("Entering " + getEnclosingMethod(enclosingFunctionObject).getName());
    }

    public static void logExit(Object enclosingFunctionObject) throws SecurityException {
        System.out.println("Exiting " + getEnclosingMethod(enclosingFunctionObject).getName());
    }

    synchronized public static void error(String msg) {
        // Log in INFO mode
        if (ApplicationSettings.isIsDevMode()) {
            // Log everything
            LogFull(msg, new Object() {
            }, Level.SEVERE);
        }
    }

    synchronized public static void warn(String msg) {
        // Log in INFO mode
        if (ApplicationSettings.isIsDevMode()) {
            // Log everything
            LogFull(msg, new Object() {
            }, Level.WARNING);
        }
    }

    synchronized public static void info(String msg) {
        // Log in INFO mode
        if (ApplicationSettings.isIsDevMode()) {
            // Log everything
            LogFull(msg, new Object() {
            }, level);
        }
    }

    private static void LogFull(String msg, Object enclosingFunctionObject, Level requestedLevel) {
        Method enclosingMethod = getEnclosingMethod(enclosingFunctionObject);
        StackTraceElement loggedFunction = getInstance().getParentFunction(enclosingMethod);
        StackTraceElement parentFunction = getInstance().getLoggedFunction(enclosingMethod);
        String paramString = getParamString(enclosingFunctionObject);

        logSimple(requestedLevel, msg + "\n => Logged from function " + getFormattedClassName(loggedFunction.getClassName()) + "::" + loggedFunction.getMethodName() + "("
                + paramString + ") called from " + getFormattedClassName(parentFunction.getClassName()) + "::" + parentFunction.getMethodName());
    }

    public static void log(Exception ex) {
        ex.printStackTrace();
    }

    synchronized public static void logSimple(String msg) {
        // Log in INFO mode
        if (ApplicationSettings.isIsDevMode()) {
            // Log everything
            _logger.log(Level.INFO, "" + msg);
        }
    }

    synchronized public static void logSimple(Level level, String msg) {
        // Log in INFO mode
        if (ApplicationSettings.isIsDevMode() || getLevel().intValue() >= level.intValue()) {
            // Log everything
            _logger.log(level, "" + msg);
        }
    }

    public static Level getLevel() {
        return level;
    }

    public static void setLevel(Level level) {
        level = level;
    }

    protected static String getParamString(Object enclosingFunctionObject) throws SecurityException {
        Parameter[] parameters = enclosingFunctionObject.getClass().getEnclosingMethod().getParameters();
        String paramString = "";
        for (int paramNr = 0; paramNr < parameters.length; paramNr++) {
            Parameter parameter = parameters[paramNr];
            paramString = paramNr > 0 ? ", " : "";
            paramString += parameter.getName();
        }
        return paramString;
    }

    protected static String getFormattedClassName(String fullClassName) {
        String[] formattedClassNameArray = fullClassName.split("\\.");
        String formattedClassName = formattedClassNameArray[formattedClassNameArray.length - 1];
        return formattedClassName;
    }

    /**
     * Gets the StackTraceElement of the first class that is not this class.
     * That should be the initial caller of a logging method.
     *
     * @return caller of the initial logging method or null if unknown.
     */
    protected StackTraceElement getParentFunction(Method enclosingMethod) {
        Throwable t = new Throwable();
        StackTraceElement[] stackTrace = t.getStackTrace();
        int index = 0;
        int stackTraceLength = stackTrace.length;

        // skip to stackentries until this class
        // skip to stackentries until this enclosingMethodName
        while (index < stackTraceLength
                && !stackTrace[index].getMethodName().equals(enclosingMethod.getName())) {
            index++;
        }

        index++;

        StackTraceElement callerStackFrame = index < stackTraceLength ? stackTrace[index] : null;
        return callerStackFrame;
    }

    protected StackTraceElement getLoggedFunction(Method enclosingMethod) {
        Throwable t = new Throwable();
        StackTraceElement[] stackTrace = t.getStackTrace();
        int index = 0;
        int stackTraceLength = stackTrace.length;

        // skip to stackentries until this class
        // skip to stackentries until this enclosingMethodName
        while (index < stackTraceLength
                && !stackTrace[index].getMethodName().equals(enclosingMethod.getName())) {
            index++;
        }

        index++;
        index++;

        StackTraceElement callerStackFrame = index < stackTraceLength ? stackTrace[index] : null;
        return callerStackFrame;
    }

    protected static GlobalLog getInstance() {
        return instance;
    }

    protected static Method getEnclosingMethod(Object enclosingFunctionObject) throws SecurityException {
        Method enclosingMethod = enclosingFunctionObject.getClass().getEnclosingMethod();
        return enclosingMethod;
    }
}
