/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.utils;

/**
 *
 * @author carl
 */
public class Search {

    private final String url;
    private final String keyWords;
    private final String location;
    private final boolean simulate;
    private final String workType;

    public Search(String newUrl, String newWorkType, String newKeyWords, String newLocation, boolean newSimulate) {
        url = newUrl;
        keyWords = newKeyWords;
        location = newLocation;
        simulate = newSimulate;
        workType = newWorkType;
    }

    public String getUrl() {
        return url;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public String getLocation() {
        return location;
    }

    public boolean isSimulate() {
        return simulate;
    }

    public String getWorkType() {
        return workType;
    }

}
