/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.text.BadLocationException;
import javax.xml.parsers.ParserConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import uk.jeesoft.application.ApplicationSettings;
import static uk.jeesoft.application.ApplicationSettings._EXTERNAL_REDIRECT_DEFINITION;
import static uk.jeesoft.application.ApplicationSettings._FOLLOW_EXTERNAL_REDIRECT;
import uk.jeesoft.jobborse.domain.lists.JobBorsePage;
import uk.jeesoft.jobborse.domain.pages.ApplicationPage;
import uk.jeesoft.selenium.navigators.jobborse.JobborseNavigator;
import static uk.jeesoft.selenium.utils.Asserter.assertThat;
import uk.jeesoft.selenium.utils.EmailBuilder;
import uk.jeesoft.selenium.utils.Persister;
import uk.jeesoft.selenium.utils.Search;

/**
 *
 * @author carl
 */
public class LinkPageHandler {

    private final static List<String> availiableLabeltexts = new ArrayList<String>(Arrays.asList(new String[]{
        "Referenznummer", "Titel des Stellenangebots", "Stellenangebotsart", "Arbeitgeber",
        "Stellenbeschreibung", "Führungsverantwortung", "Arbeitsorte", "Beginn der Tätigkeit",
        "Anzahl offener Stellen", "Rückfragen und Bewerbungen an", "E-Mail",
        "Gewünschte Bewerbungsarten", "Internetadresse", "Bewerbungszeitraum", "Angaben zur Bewerbung"
    }));

    private final static List<String> selectedLabeltexts = new ArrayList<String>(Arrays.asList(new String[]{
        "Referenznummer", "Titel des Stellenangebots", "Arbeitgeber",
        "Stellenbeschreibung", "Arbeitsorte", "Beginn der Tätigkeit",
        "Rückfragen und Bewerbungen an", "E-Mail",
        "Internetadresse", "Angaben zur Bewerbung"
    }));

    private static final HashMap<String, Object> availiableLabeltextMap = new HashMap<>();


    private static JobBorsePage pageInstance;
    public static List<WebElement> getResultLinksList() throws InterruptedException, IOException {
        String nrOfHitsString = JobborseNavigator.retryingFindClick(By.xpath("//div[@class='ergebnisNavigation zweispaltig cf']")).getText();
        System.out.println("nrOfHitsString= " + nrOfHitsString);
        WebElement table = JobborseNavigator.findElement(By.xpath("//table[@id='tabellenanfang']"));
        List<WebElement> links = table.findElements(By.xpath("//table[@id='tabellenanfang']/tbody/tr/td[2]/div/a"));
        return links;
    }

    static void add(WebElement link) {
        getInstance().add(link);
    }

    private static JobBorsePage getInstance() {
        if (pageInstance == null) {
            pageInstance = new JobBorsePage(new Date().toString());
        }
        return pageInstance;
    }


    static JobBorsePage persistLinks(String arbeitsort) throws IOException {
        if (pageInstance != null) {
            pageInstance.setName(arbeitsort);
            Persister.persist(pageInstance);
        }
        return pageInstance;
    }

    static void handleLinks(List<WebElement> links) {

        for (WebElement link : links) {
            add(link);
//            link.click();
        }
    }


    static void handleSelectedLink(String linkUrl) throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {
        JobborseNavigator.openUrl(linkUrl);
        String referenznummer = null;
        WebElement eingabemaske = null;
        boolean isAStandardPage = true;
        try {
            eingabemaske = JobborseNavigator.retryingFindClick(By.xpath("//div[@id='eingabemaske']"));
        } catch (NoSuchElementException e) {
            isAStandardPage = false;
        }
        ApplicationPage page = null;
        if (isAStandardPage) {
            List<WebElement> dataSections = eingabemaske.findElements(By.xpath(".//div[@class='cf']"));

            Map<String, String> pageBodyMap = new TreeMap<>();

            for (WebElement dataSection : dataSections) {
                if (dataSection.getTagName().toLowerCase().equals("div")) {
                    String html = dataSection.getAttribute("innerHTML");
                    Document doc = Jsoup.parse(html);
                    Element labelTextElement = doc.selectFirst("span[class='labelText ohneFeldhilfe']");

                    Element infoTextElement = doc.selectFirst("a[id^='vermittlung.stellenangeboteverwalten.detailszumstellenangebot']");
                    if (infoTextElement == null) {
                        infoTextElement = doc.selectFirst("p");
                    }
                    if (labelTextElement != null && infoTextElement != null) {
                        String labelText = labelTextElement.text();
                        String infoText = infoTextElement.text();
//                        System.out.println("saving : " + labelText + ", " + infoText);
                        pageBodyMap.put(labelText, infoText);
                    }
                }
            }
            HashMap<String, Object> tmpAvailiableLabeltextMap = new HashMap<>(availiableLabeltextMap);
            for (String key : pageBodyMap.keySet()) {
                tmpAvailiableLabeltextMap.remove(key);
            }
            assertThat(tmpAvailiableLabeltextMap).isEmpty();

            page = new ApplicationPage(pageBodyMap);
            referenznummer = page.getId();

        } else {
            referenznummer = JobborseNavigator.getTitle();
            page = new ApplicationPage(referenznummer);
        }
        page.setUrl(linkUrl);
        if (referenznummer != null) {
            Persister.persist(page);
            EmailBuilder.LogPageBuilt(page);
        }
    }

    static JobBorsePage retrieveFromBrowser(Search search, int maxNrOfPages)
            throws IOException, InterruptedException, SAXException, BadLocationException, ParserConfigurationException {
        JobBorsePage page = null;
        try {
            page = retrieveFromBrowser(search.getUrl(), search.getWorkType(), search.getKeyWords(), search.getLocation(), search.isSimulate(), maxNrOfPages);
        } catch (NullPointerException e) {
            System.out.println("Exception !- " + e.getMessage());
        }
        return page;
    }

    private static JobBorsePage retrieveFromBrowser(String linkUrl, String workType, String suchbegriffe,
            String arbeitsort, boolean doActuallyRetrieve, int maxNrOfPages) throws IOException, InterruptedException, SAXException, BadLocationException, ParserConfigurationException {
        SearchHandler.handleFirstPage(linkUrl, workType, suchbegriffe, arbeitsort);

        SearchHandler.handleAdvancedSearch();
        JobBorsePage persisted = null;
        if (doActuallyRetrieve) {
            List<WebElement> links;
            int pageNr = 0;
            do {
                links = LinkPageHandler.getResultLinksList();
                LinkPageHandler.handleLinks(links);
            } while (JobborseNavigator.hasMorePages() && JobborseNavigator.nextPage()
                    && maxNrOfPages == ApplicationSettings._ALL || ++pageNr < maxNrOfPages);

            links = LinkPageHandler.getResultLinksList();
            LinkPageHandler.handleLinks(links);
            persisted = LinkPageHandler.persistLinks(arbeitsort);
            if (persisted != null) {
                retrievePages(persisted);
            }

        }
        return persisted;
    }

    public static void retrievePages(JobBorsePage page) throws SAXException, InterruptedException, IOException, BadLocationException, ParserConfigurationException {
        for (String link : page.getLinksList()) {
            if (link.contains(_EXTERNAL_REDIRECT_DEFINITION)) {
                if (_FOLLOW_EXTERNAL_REDIRECT) {
                    LinkPageHandler.handleSelectedLink(link);
                }
            } else {

                LinkPageHandler.handleSelectedLink(link);
            }
        }
        report();
    }

    public static void report() throws FileNotFoundException, IOException {
        List<ApplicationPage> pages = Persister.getAll(ApplicationPage.class);
        EmailBuilder.buildEmailList(pages, "MailList-Frankfurt-area");
        EmailBuilder.persistPageLog(pages, "MailList-Frankfurt-area");
    }
}
