package uk.jeesoft.selenium;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import uk.jeesoft.application.ApplicationSettings;

public class Init {

    private static String _browserURL;

    public static void doInit() {

        File file = getDriverFile();
        file.setReadable(true, true);
        file.setWritable(true, true);
        file.setExecutable(true, false);
    }

    public static void initDriver() {
        String filePath = getDriverFile().getAbsolutePath();
        System.setProperty(ApplicationSettings._SELECTED_WEBDRIVER_SYSTEM_PROPERTY, filePath);

//        Process process = Runtime.getRuntime().exec("chmod +x '" + filePath + "'");
//        process.waitFor();
    }

    private static File getDriverFile() {
        URL url = ClassLoader.getSystemResource(ApplicationSettings._SELECTED_WEBDRIVER);
        File file = new File(url.getFile());
        return file;
    }

    public static WebDriver createDriver() throws IllegalStateException, IOException, InterruptedException {
        WebDriver tmpDriver = null;
        ChromeOptions options = new ChromeOptions();
        // if you like to specify another profile
        options.addArguments("--disable-notifications");
        options.addArguments("--auto-open-devtools-for-tabs");
        try {
            tmpDriver = new ChromeDriver(options);
//            tmpDriver = new FirefoxDriver();

        } catch (IllegalStateException exception) {
            if (exception.getMessage().startsWith("The driver is not executable")) {
                Init.doInit();
                tmpDriver = new ChromeDriver();
//                tmpDriver = new FirefoxDriver();
            } else {
                throw exception;
            }
        }
        tmpDriver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        tmpDriver.manage().window().maximize();
        tmpDriver.get(getBrowserURL());
        return tmpDriver;
    }

    public static String getBrowserURL() {
        return _browserURL;
    }

    public static void setBrowserURL(String _loginURL) {
        _browserURL = _loginURL;
    }

}
