/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import uk.jeesoft.jobborse.domain.Persistable;
import uk.jeesoft.jobborse.domain.pages.ApplicationPage;
import static uk.jeesoft.selenium.utils.ApplicationUtil._BASE_DIRECTORY;
import static uk.jeesoft.selenium.utils.ApplicationUtil.getPathToSave;

/**
 *
 * @author carl
 */
public class Persister {

    public static <P extends Persistable> void persist(P persistable) throws IOException {
        String fileName = ApplicationUtil.calculateFileName(persistable);
        File file = new File(fileName);
        System.out.println("Tries to write to file: " + file.getAbsolutePath());

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }

//Object to JSON in file
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, persistable);
    }

    public static <P extends Persistable> P retrieve(String id, Class<P> instanceClass) throws IOException {

        String pathToSave = getPathToSave(instanceClass);
        File file = new File(pathToSave + id + ".json");

        P instance = mapJsonToObject(file, instanceClass);
        return instance;
    }

    public static <P extends Persistable> P delete(String id, Class<P> instanceClass) throws IOException {
        String pathToSave = getPathToSave(instanceClass);
        P instance = retrieve(id, instanceClass);
        File file = new File(pathToSave + id + ".json");
        file.delete();
        return instance;
    }

    public static void clearAll() {
        File dir = new File(_BASE_DIRECTORY);
        deleteDirectory(dir);
    }

    public static boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        } // either file or an empty directory
        System.out.println("removing file or directory : " + dir.getName());
        return dir.delete();
    }

    private static <P extends Persistable> P mapJsonToObject(File file, Class<P> objectClass) throws IOException {
        //JSON from file to Object
        ObjectMapper mapper = new ObjectMapper();
        P page = mapper.readValue(file, objectClass);
        return page;
    }

    public static List<ApplicationPage> getAll(Class<ApplicationPage> instanceClass) throws IOException {
        String pathToSave = getPathToSave(instanceClass);

        File dir = new File(pathToSave);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }
        List<ApplicationPage> applicationPageList = new ArrayList<>();
        for (File file : files) {
            try {
                ApplicationPage instance = mapJsonToObject(file, instanceClass);
                applicationPageList.add(instance);
            } catch (FileNotFoundException exception) {
                System.out.println(exception.getMessage());
            }
        }
        return applicationPageList;
    }
}
