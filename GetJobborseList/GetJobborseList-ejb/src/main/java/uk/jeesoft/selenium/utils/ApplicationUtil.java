/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.utils;

import uk.jeesoft.jobborse.domain.Persistable;

/**
 *
 * @author carl
 */
public class ApplicationUtil {

    public static String _BASE_DIRECTORY = "/tmp/Jobborse/";

    public static <P extends Persistable> String calculateFileName(P persistable) {
        String id = null;
        id = persistable.getId();
        String name = null;
        name = persistable.getName() != null ? persistable.getName() : "";
        String pathToSave = getPathToSave(persistable.getClass());
        String fileName = pathToSave + (!name.isEmpty() ? name + "_" : "") + id + ".json";
        return fileName;
    }

    public static <P extends Persistable> String getPathToSave(Class<P> instanceClass) {
        String packageName = instanceClass.getPackage().getName();
        String pathToSave = _BASE_DIRECTORY + packageName + "/";
        return pathToSave;
    }
}
