/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.navigators.jobborse;

import java.io.IOException;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.jeesoft.selenium.navigators.ChromeNavigator;

/**
 *
 * @author carl
 */
public class JobborseChromeNavigator extends ChromeNavigator {

    private WebDriver _driver;
    private ArrayList<String> tabs2;
    private String presentTab;
    private int pageNr = 1;

    public JobborseChromeNavigator() throws IOException, InterruptedException {

    }

    boolean hasMorePages() throws IOException, InterruptedException {
        By by = By.xpath("//div[@class='ergebnisNavigation zweispaltig cf']");
        WebElement ergebnisPaginierung = retryingFindClick(by);
        by = By.xpath("//a[starts-with(@title,'zu Seite " + (pageNr + 1) + " ')]");
        try {
            ergebnisPaginierung.findElement(by);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    int nextPage() throws IOException, InterruptedException {

        By by = By.xpath("//div[@class='ergebnisNavigation zweispaltig cf']");
        WebElement ergebnisPaginierung = retryingFindClick(by);
        by = By.xpath("//a[starts-with(@title,'zu Seite " + ++pageNr + " ')]");
        ergebnisPaginierung.findElement(by).click();
        return pageNr;
    }

    int getPageNr() {
        return pageNr;
    }

    void unFold(By by) throws IOException, InterruptedException {
        try {
            WebElement element = retryingFindClick(by);
            element.findElement(By.xpath("//img[@title='Abschnitt einblenden']"));
            retryingFindClick(by);
        } catch (NoSuchElementException e) {
            System.out.println("Unfolded " + by);
        }
    }
}
