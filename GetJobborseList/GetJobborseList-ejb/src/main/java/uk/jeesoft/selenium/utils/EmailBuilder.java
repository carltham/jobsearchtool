/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import uk.jeesoft.jobborse.domain.pages.ApplicationPage;
import static uk.jeesoft.selenium.utils.ApplicationUtil._BASE_DIRECTORY;

/**
 *
 * @author carl
 */
public class EmailBuilder {

    private static PageLog pageLog;

    public static String buildEmailList(List<ApplicationPage> pages, String filename) throws FileNotFoundException {
        StringBuilder exportable = new StringBuilder();

        createRow(exportable, "Email");
        Map<String, String> emailMap = new TreeMap<>();
        for (ApplicationPage page : pages) {
            String email = page.geteMail();
            if (email != null && emailMap.get(email) == null) {
                emailMap.put(email, email);
                createRow(exportable, email);
            }
        }
        String today = getTodaysDate();

        PrintWriter pw = new PrintWriter(new File(_BASE_DIRECTORY + filename + "-" + today + "MailList.csv"));
        String emailList = exportable.toString();
        pw.write(emailList);
        pw.close();
        System.out.println("done!");
        return emailList;
    }

    public static void LogPageBuilt(ApplicationPage page) {
        getPageLog().add(page);
    }

    public static void persistPageLog(List<ApplicationPage> pages, String filename) throws FileNotFoundException {
        StringBuilder exportable = new StringBuilder();
        createRow(exportable, "Page", "Times");
        HashMap<String, Integer> fileNameMap = getPageLog().getFileNameMap();
        for (Map.Entry<String, Integer> entry : fileNameMap.entrySet()) {
            String page = entry.getKey();
            Integer times = entry.getValue();
            createRow(exportable, page, "" + times);
        }
        String today = getTodaysDate();

        PrintWriter pw = new PrintWriter(new File(_BASE_DIRECTORY + filename + "-" + today + "PageLog.csv"));
        String emailList = exportable.toString();
        pw.write(emailList);
        pw.close();
        System.out.println("done!");
    }

    private static String getTodaysDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String today = formatter.format(new Date());
        return today;
    }

    private static void createRow(StringBuilder exportable, String... entries) {
        for (int nr = 0; nr < entries.length; nr++) {
            String entry = entries[nr];
            exportable.append(entry);
            if (nr < entries.length) {
                exportable.append(",");
            }
        }
        exportable.append('\n');
    }

    private static PageLog getPageLog() {
        if (pageLog == null) {
            pageLog = new PageLog();
        }
        return pageLog;
    }

}
