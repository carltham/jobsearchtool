/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.utils;

import java.util.HashMap;
import uk.jeesoft.jobborse.domain.pages.ApplicationPage;

/**
 *
 * @author carl
 */
public class PageLog {

    private HashMap<String, Integer> fileNameMap;

    public void add(ApplicationPage page) {
        String fileName = ApplicationUtil.calculateFileName(page);
        int counter = getFileNameMap().get(fileName) != null ? getFileNameMap().get(fileName) : 0;
        getFileNameMap().put(fileName, ++counter);
    }

    public HashMap<String, Integer> getFileNameMap() {
        if (fileNameMap == null) {
            fileNameMap = new HashMap<>();
        }
        return fileNameMap;
    }

}
