/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.tests;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import uk.jeesoft.selenium.navigators.jobborse.JobborseNavigator;

/**
 *
 * @author carl
 */
public class SearchHandler {

    static void handleFirstPage(String linkUrl, String workType, String suchbegriffe, String arbeitsort) throws IOException, InterruptedException {
        System.out.println("Searching for Job with keyword(s) " + suchbegriffe + " in " + arbeitsort + "!!");
        JobborseNavigator.openUrl(linkUrl);

        WebElement field = null;
        try {
            field = JobborseNavigator.retryingFindClick(By.id("siesuchen"));
            new Select(field).selectByVisibleText(workType);
            JobborseNavigator.fillBy(By.id("suchbegriffe"), suchbegriffe);
            JobborseNavigator.fillBy(By.id("arbeitsort"), arbeitsort + "\t\t");
//            JobborseNavigator.retryingFindClick(By.xpath("a[class='ui-corner-all']"));
            JobborseNavigator.retryingFindClick(By.id("erweitertesuche"));
        } catch (NoSuchElementException exception) {
            System.out.println("NoSuchElementException : " + exception.getMessage());
            throw exception;
        }
    }

    static void handleAdvancedSearch() throws IOException, InterruptedException {
        JobborseNavigator.unFold(By.xpath("//a[.='Weitere Suchkriterien']"));
        JobborseNavigator.retryingFindClick(By.id("branchengruppenauswahlen"));

        JobborseNavigator.retryingFindClick(By.xpath("//tr[contains(.,'Arbeitnehmerüberlassung, Zeitarbeit')]//input[@type='checkbox']"));
        JobborseNavigator.retryingFindClick(By.xpath("//tr[contains(.,'Arbeitsvermittlung, privat')]//input[@type='checkbox']"));
        JobborseNavigator.retryingFindClick(By.id("ubernehmen_unten"));
        JobborseNavigator.retryingFindClick(By.id("stellenangebotesuchen_oben"));
    }
}
