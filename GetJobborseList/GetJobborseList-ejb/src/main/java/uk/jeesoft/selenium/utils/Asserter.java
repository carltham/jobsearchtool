/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.utils;

import java.util.HashMap;

/**
 *
 * @author carl
 */
public class Asserter {

    private static Asserter instance;


    public static Asserter assertThat(HashMap<String, Object> map) {
        getInstance().underTest = map;
        return getInstance();
    }

    private static Asserter getInstance() {
        if (instance == null) {
            instance = new Asserter();
        }
        return instance;
    }
    private HashMap<String, Object> underTest;

    public void isEmpty() {
        if (underTest.size() > 0) {
            throw new AssertionError("The provided mapis not empty");
        }
    }

}
