/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.navigators;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author carl
 */
public class ChromeNavigator {

    private WebDriver _driver;

    public final void quit() throws IOException, InterruptedException {
//        getDriver().close();
        getDriver().quit();
    }

    public final WebDriver getDriver() {
        return _driver;
    }

    public final void setDriver(WebDriver newDriver) {
        this._driver = newDriver;
    }

    public final String getTitle() throws IOException, InterruptedException {
        return getDriver().getTitle();
    }

    public final WebElement findElement(By by) throws IOException, InterruptedException {
//        return driver.findElement(By.xpath("//table[@id='tabellenanfang']"));
        return getDriver().findElement(by);
    }

    public final void fillBy(By by, String value) throws IOException, InterruptedException {
        WebElement field = retryingFindClick(by);
        field.clear();
        field.sendKeys(value);
    }

    public final WebElement retryingFindClick(By by) throws IOException, InterruptedException {
        WebElement foundElement = null;
        int attempts = 0;
        while (++attempts < 5 && foundElement == null) {
            try {
                Thread.sleep(200);

                foundElement = getDriver().findElement(by);

                Actions action = new Actions(getDriver()).moveToElement(foundElement);
                action.click().perform();
            } catch (StaleElementReferenceException e) {
                System.out.println(e.getMessage());
            } catch (NoSuchElementException e) {
                System.out.println(e.getMessage());
                throw e;
            }
        }

        return foundElement;
    }

    public void openUrl(String url) throws IOException, InterruptedException {
        getDriver().get(url);
    }

}
