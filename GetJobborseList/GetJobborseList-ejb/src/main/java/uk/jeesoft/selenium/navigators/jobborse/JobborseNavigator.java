/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.navigators.jobborse;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jaya
 */
public class JobborseNavigator {


    private static JobborseChromeNavigator navigatiorInstance;
    public static void quit() throws IOException, InterruptedException {
        getInstance().quit();
    }

    public static String getTitle() throws IOException, InterruptedException {
        return getInstance().getTitle();
    }

    public static WebElement findElement(By by) throws IOException, InterruptedException {
        return getInstance().findElement(by);
    }

    public static void fillBy(By by, String value) throws IOException, InterruptedException {
        getInstance().fillBy(by, value);
    }

    public static WebElement retryingFindClick(By by) throws IOException, InterruptedException {
        return getInstance().retryingFindClick(by);
    }

    private static JobborseChromeNavigator getInstance() throws IOException, InterruptedException {
        if (navigatiorInstance == null) {
            navigatiorInstance = new JobborseChromeNavigator();
        }
        return navigatiorInstance;
    }

    public static boolean hasMorePages() throws IOException, InterruptedException {
        return getInstance().hasMorePages();
    }

    public static boolean nextPage() throws IOException, InterruptedException {
        try {
            getInstance().nextPage();

        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public static WebDriver getDriver() throws IOException, InterruptedException {
        return getInstance().getDriver();
    }

    public static void setDriver(WebDriver newDriver) throws IOException, InterruptedException {
        getInstance().setDriver(newDriver);
    }

    public static int getPageNr() throws IOException, InterruptedException {
        return getInstance().getPageNr();
    }

    public static void openUrl(String linkUrl) throws IOException, InterruptedException {
        getInstance().openUrl(linkUrl);
    }

    public static void unFold(By by) throws IOException, InterruptedException {
        getInstance().unFold(by);
    }
}
