/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.jobborse.domain.pages;

import java.util.Map;
import uk.jeesoft.jobborse.domain.Persistable;

/**
 *
 * @author carl
 */
public class ApplicationPage implements Persistable {
    private static int objectCounter;

    private String id;
    private String titel;
    private String name;
    private String stellenbeschreibung;
    private String arbeitsorte;
    private String beginn;
    private String kontakPerson;
    private String eMail;
    private String internetadresse;
    private String geforderteAngaben;
    private String url;

    public ApplicationPage(Map<String, String> pageBodyMap) {

        id = escaped(pageBodyMap.get("Referenznummer"));
        name = pageBodyMap.get("Arbeitgeber");
        titel = pageBodyMap.get("Titel des Stellenangebots");
        stellenbeschreibung = pageBodyMap.get("Stellenbeschreibung");
        arbeitsorte = pageBodyMap.get("Arbeitsorte");
        beginn = pageBodyMap.get("Beginn der Tätigkeit");
        kontakPerson = pageBodyMap.get("Rückfragen und Bewerbungen an");
        eMail = pageBodyMap.get("E-Mail");
        internetadresse = pageBodyMap.get("Internetadresse");
        geforderteAngaben = pageBodyMap.get("Angaben zur Bewerbung");
    }

    public ApplicationPage(String newReferenznummer) {
        id = getObjectCounter() + "-" + newReferenznummer;
        name = null;
        titel = null;
        stellenbeschreibung = null;
        arbeitsorte = null;
        beginn = null;
        kontakPerson = null;
        eMail = null;
        internetadresse = null;
        geforderteAngaben = null;
    }

    public ApplicationPage() {
        id = null;
        name = null;
        titel = null;
        stellenbeschreibung = null;
        arbeitsorte = null;
        beginn = null;
        kontakPerson = null;
        eMail = null;
        internetadresse = null;
        geforderteAngaben = null;
    }

    private int getObjectCounter() {
        return objectCounter++;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getStellenbeschreibung() {
        return stellenbeschreibung;
    }

    public void setStellenbeschreibung(String stellenbeschreibung) {
        this.stellenbeschreibung = stellenbeschreibung;
    }

    public String getArbeitsorte() {
        return arbeitsorte;
    }

    public void setArbeitsorte(String arbeitsorte) {
        this.arbeitsorte = arbeitsorte;
    }

    public String getBeginn() {
        return beginn;
    }

    public void setBeginn(String beginn) {
        this.beginn = beginn;
    }

    public String getKontakPerson() {
        return kontakPerson;
    }

    public void setKontakPerson(String kontakPerson) {
        this.kontakPerson = kontakPerson;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getInternetadresse() {
        return internetadresse;
    }

    public void setInternetadresse(String internetadresse) {
        this.internetadresse = internetadresse;
    }

    public String getGeforderteAngaben() {
        return geforderteAngaben;
    }

    public void setGeforderteAngaben(String geforderteAngaben) {
        this.geforderteAngaben = geforderteAngaben;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String escaped(String raw) {
        String escaped = null;
        try {
            escaped = raw.replace("/", "#slashed#");
        } catch (NullPointerException exception) {
            escaped = getObjectCounter() + "- external";
            System.out.println("NullPointerException : " + exception.getMessage());
        }
        return escaped;
    }

}
