/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.jobborse.domain.lists;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;
import uk.jeesoft.jobborse.domain.Persistable;

/**
 *
 * @author carl
 */
public class JobBorsePage implements Persistable {

    private String id;

    private String name;

    private List<String> linksList = new ArrayList<>();

    public JobBorsePage() {
    }

    public JobBorsePage(String newId) {
        this.id = newId;
    }

    public void add(WebElement link) {
        String href = link.getAttribute("href");
        linksList.add(href);
    }

    public void listLinks() {
        for (String href : linksList) {
            System.out.println("href = " + href);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLinksList() {
        return linksList;
    }

    public void setLinksList(List<String> linksList) {
        this.linksList = linksList;
    }

}
