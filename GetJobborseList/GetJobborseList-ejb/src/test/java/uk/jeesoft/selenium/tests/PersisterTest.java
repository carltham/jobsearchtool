/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.selenium.tests;

import org.junit.Test;
import uk.jeesoft.jobborse.domain.lists.JobBorsePage;
import uk.jeesoft.selenium.utils.Persister;

/**
 *
 * @author carl
 */
public class PersisterTest {

    private final String _TEST_ID_ = "testId";

    /**
     * Test of persist method, of class Persister.
     */
    @Test
    public void testPersist() throws Exception {
        System.out.println("persist");
        Persister.clearAll();
        JobBorsePage persistable = new JobBorsePage(_TEST_ID_);
        Persister.persist(persistable);
        Persister.delete(_TEST_ID_, persistable.getClass());
    }

}
