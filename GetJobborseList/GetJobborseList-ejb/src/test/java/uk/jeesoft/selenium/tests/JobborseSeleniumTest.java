package uk.jeesoft.selenium.tests;

import java.io.IOException;
import javax.swing.text.BadLocationException;
import javax.xml.parsers.ParserConfigurationException;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.xml.sax.SAXException;
import uk.jeesoft.application.ApplicationSettings;
import uk.jeesoft.jobborse.domain.lists.JobBorsePage;
import uk.jeesoft.log.GlobalLog;
import uk.jeesoft.selenium.Init;
import uk.jeesoft.selenium.navigators.jobborse.JobborseNavigator;
import uk.jeesoft.selenium.utils.Persister;
import uk.jeesoft.selenium.utils.Search;

public class JobborseSeleniumTest {

    @BeforeClass
    public static void openBrowser() throws IOException, InterruptedException {
        Init.setBrowserURL(ApplicationSettings.jobborseURL);
        Init.initDriver();
        WebDriver tmpDriver = Init.createDriver();
        JobborseNavigator.setDriver(tmpDriver);
    }

    @AfterClass
    public static void closeBrowser() throws IOException, InterruptedException {
        JobborseNavigator.quit();
    }

    @Test
    public void findTitle() throws IOException, InterruptedException {
        GlobalLog.logEnter(new Object() {
        });
        String pageTitle = JobborseNavigator.getTitle();
        System.out.println("Application title is :: " + pageTitle);
        String expectedTitle = "JOBBÖRSE ";
        assertThat(pageTitle).startsWith(expectedTitle);
        GlobalLog.logExit(new Object() {
        });
    }

    @Test
    public void getPagesFromOneArea() throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {
        GlobalLog.logEnter(new Object() {
        });
        Persister.clearAll();
        LinkPageHandler.retrieveFromBrowser(ApplicationSettings._SEARCHES_AREA.get("Frankfurt am Main"), 1);
        GlobalLog.logExit(new Object() {
        });
    }

    @Test
    public void getPagesFromAllArea() throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {
        GlobalLog.logEnter(new Object() {
        });
        Persister.clearAll();
        ApplicationSettings.loadSearchArea(ApplicationSettings._SEARCHES_ONLY_MUNICH_JAVA);
        for (Search search : ApplicationSettings._SEARCHES_AREA.values()) {
            LinkPageHandler.retrieveFromBrowser(search, 1);
        }
        GlobalLog.logExit(new Object() {
        });
    }

    @Test
    public void retrievePages() throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {
        GlobalLog.logEnter(new Object() {
        });
        Persister.clearAll();
        JobBorsePage page = LinkPageHandler.retrieveFromBrowser(ApplicationSettings._SEARCHES_ONLY_FRANKFURT_JAVA[0], 1);

        LinkPageHandler.retrievePages(page);
        GlobalLog.logExit(new Object() {
        });
    }

    @Test
    public void report() throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {
        GlobalLog.logEnter(new Object() {
        });
        Persister.clearAll();
        JobBorsePage page = LinkPageHandler.retrieveFromBrowser(ApplicationSettings._SEARCHES_ONLY_FRANKFURT_JAVA[0], 1);
        LinkPageHandler.retrievePages(page);

        LinkPageHandler.report();
        GlobalLog.logExit(new Object() {
        });
    }

}
