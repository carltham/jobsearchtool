package uk.jeesoft.selenium.tests;

import java.io.IOException;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;
import uk.jeesoft.jobborse.domain.pages.ApplicationPage;
import uk.jeesoft.jobborse.domain.pages.ApplicationPageBuilder;
import uk.jeesoft.log.GlobalLog;
import uk.jeesoft.selenium.utils.EmailBuilder;
import uk.jeesoft.selenium.utils.Persister;

public class JobborseTest {

    private static final String browserURL = "https://jobboerse.arbeitsagentur.de/vamJB/startseite.html?aa=1&m=1&kgr=as&vorschlagsfunktionaktiv=true";

    @Before
    public void startTest() {
    }

    @After
    public void stopTest() {
    }

    @Test
    public void shouldRetrieveAllPersistedJobs() throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {

        GlobalLog.logEnter(new Object() {
        });
        prepareTest();
        List<ApplicationPage> pages = Persister.getAll(ApplicationPage.class);
        System.out.println("Email\t\t\t\t\t\tkontakPerson");
        for (ApplicationPage page : pages) {
            System.out.println(page.geteMail() + "\t\t\t\t\t\t" + page.getKontakPerson());
        }

        GlobalLog.logExit(new Object() {
        });
    }

    @Test
    public void shouldCreateAnEmailList_FromPersistedJobs() throws IOException, InterruptedException, SAXException, ParserConfigurationException, BadLocationException {

        GlobalLog.logEnter(new Object() {
        });
        prepareTest();
        List<ApplicationPage> pages = Persister.getAll(ApplicationPage.class);
        EmailBuilder.buildEmailList(pages, "Test-MailList-Frankfurt-area");

        GlobalLog.logExit(new Object() {
        });
    }

    private void prepareTest() throws IOException {
        Persister.clearAll();
        ApplicationPage page = new ApplicationPageBuilder().withId("test" + 99).withEmail("test" + 2 + "@test.now").build();
        Persister.persist(page);
        persistPage(3);
    }

    private void persistPage(int nrOfPages) throws IOException {
        GlobalLog.logEnter(new Object() {
        });
        ApplicationPage page;
        for (int pageNr = nrOfPages; pageNr > 0; pageNr--) {
            page = new ApplicationPageBuilder().withId("test" + pageNr).withEmail("test" + pageNr + "@test.now").build();
            Persister.persist(page);
        }

        GlobalLog.logExit(new Object() {
        });
    }

}
