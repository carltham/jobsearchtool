/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.jobborse.domain.pages;

/**
 *
 * @author carl
 */
public class ApplicationPageBuilder {

    private ApplicationPage page;

    public ApplicationPageBuilder() {
    }

    public ApplicationPage getPage() {
        if (page == null) {
            page = new ApplicationPage();
        }
        return page;
    }

    public ApplicationPageBuilder withEmail(String newEmail) {
        getPage().seteMail(newEmail);
        return this;
    }

    public ApplicationPageBuilder withId(String newId) {
        getPage().setId(newId);
        return this;
    }

    public ApplicationPage build() {
        return getPage();
    }

}
