package uk.jeesoft.selenium.tests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carl
 */
import uk.jeesoft.selenium.Init;
import java.io.IOException;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class FacebookTitleTest {
    private static String browserURL= "http://facebook.com/";
    private static WebDriver driver;

    @BeforeClass
    public static void openBrowser() throws IOException, InterruptedException {
        Init.setBrowserURL(browserURL);
            Init.initDriver();
             driver = Init.createDriver();
    }

    @AfterClass
    public static void closeBrowser() throws IOException, InterruptedException {
        driver.quit();
    }

    @Test
    public void pageTitleAfterSearchShouldBeThere() throws IOException, InterruptedException {
        System.out.println(driver.getTitle());
        assertEquals("The page title should be this at the start of the test.", "Facebook - Log In or Sign Up", driver.getTitle());
    }
}