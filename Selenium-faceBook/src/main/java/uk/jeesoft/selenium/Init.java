package uk.jeesoft.selenium;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Init {

    private static String _browserURL;

    public static void doInit() throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec("chmod +x '/DATA/Projects/poc/Jobborse/target/classes/chromedriver'");
        process.waitFor();

        File file = getDriverFile();
        file.setReadable(true, true);
        file.setWritable(true, true);
        file.setExecutable(true, false);
    }

    public static void initDriver() {
        String filePath = getDriverFile().getAbsolutePath();
//        System.setProperty("webdriver.gecko.driver", filePath);
        System.setProperty("webdriver.chrome.driver", filePath);
    }

    private static File getDriverFile() {
        URL url = ClassLoader.getSystemResource("chromedriver");
        File file = new File(url.getFile());
        return file;
    }

    public static WebDriver createDriver() throws IllegalStateException, IOException, InterruptedException {
        ChromeDriver tmpDriver = null;
        ChromeOptions options = new ChromeOptions();
        // if you like to specify another profile
//        options.addArguments("start-maximized");
//        options.addArguments("user-data-dir=/tmp/Jobborse/aaa");

        options.addArguments("--disable-notifications");
        options.addArguments("--auto-open-devtools-for-tabs");
        try {
            tmpDriver = new ChromeDriver(options);

        } catch (IllegalStateException exception) {
            if (exception.getMessage().startsWith("The driver is not executable")) {
                Init.doInit();
                tmpDriver = new ChromeDriver();
            } else {
                throw exception;
            }
//            java.lang.: The driver is not executable: /DATA/Projects/poc/Jobborse/target/classes/chromedriver
        }
        tmpDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        tmpDriver.manage().window().maximize();
        tmpDriver.get(getBrowserURL());
        return tmpDriver;
    }

    public static String getBrowserURL() {
        return _browserURL;
    }

    public static void setBrowserURL(String _loginURL) {
        _browserURL = _loginURL;
    }

}
