// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function (tab) {
    console.log("Started printing");
    
    chrome.tabs.executeScript(tabId, {
        //..
    }, _ => {
        let e = chrome.runtime.lastError;
        if (e !== undefined) {
            console.log(tabId, _, e);
        }
    });
    var action_url = "javascript:window.print();";
    chrome.tabs.update(tab.id, {url: action_url});
    console.log("Finished printing");
});
