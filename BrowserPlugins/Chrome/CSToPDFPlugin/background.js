// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function (tab) {
    console.log("Started printing");
//    var action_url = "javascript:window.print();";

    var pdf = new jsPDF('p', 'pt', 'letter');
    var canvas = pdf.canvas;

    canvas.width = 8.5 * 72;

    html2canvas(document.body, {
        canvas: canvas,
        onrendered: function (canvas) {
            var iframe = document.createElement('iframe');
            iframe.setAttribute('style', 'position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
            document.body.appendChild(iframe);
            iframe.src = pdf.output('datauristring');

            //var div = document.createElement('pre');
            //div.innerText=pdf.output();
            //document.body.appendChild(div);
        }
    });
//    chrome.tabs.update(tab.id, {url: action_url});
    console.log("Finished printing");
});
