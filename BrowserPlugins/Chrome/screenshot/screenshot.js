// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
var pdfToRender;
function setScreenshotUrl(url) {
    console.log("In view.setScreenshotUrl(url) - Begin");
    document.getElementById('target').src = url;
    console.log("In view.setScreenshotUrl(url) - End");
}

function setPdf(pdf) {
    console.log("In view.setPdf(pdf) - Begin");

    pdfToRender = pdf;
    console.log("In view.setPdf(pdf) - End");
}
function renderPdf() {
    console.log("In view.renderPdf() - Begin");

    var iframe = document.createElement('iframe');
//            var iframe = document.createElement('iframe');
    iframe.setAttribute('style', 'position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
    iframe.src = chrome.extension.getURL('frame.html');//pdfToRender.output('datauristring');

    // Some styles for a fancy sidebar
    iframe.style.cssText = 'position:fixed;top:0;left:0;display:block;' +
            'width:300px;height:100%;z-index:1000;';
    document.body.appendChild(iframe);

    var embed = document.createElement('embed');
    embed.src = chrome.extension.getURL('frame.html');//pdfToRender.output('datauristring');
    embed.type = 'application/pdf';  // Optional
//    document.body.appendChild(embed);

    console.log("In view.renderPdf() - End");
}
