// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// To make sure we can uniquely identify each screenshot tab, add an id as a
// query param to the url that displays the screenshot.
// Note: It's OK that this is a global variable (and not in localStorage),
// because the event page will stay open as long as any screenshot tabs are
// open.
var id = 100;

// Listen for a click on the camera icon. On that click, take a screenshot.
chrome.browserAction.onClicked.addListener(function () {

    chrome.tabs.captureVisibleTab(function (screenshotUrl) {
        var viewTabUrl = chrome.extension.getURL('screenshot.html?id=' + id++)
        var targetId = null;

        chrome.tabs.onUpdated.addListener(function listener(tabId, changedProps) {
            // We are waiting for the tab we opened to finish loading.
            // Check that the tab's id matches the tab we opened,
            // and that the tab is done loading.
            if (tabId != targetId || changedProps.status != "complete")
                return;

            // Passing the above test means this is the event we were waiting for.
            // There is nothing we need to do for future onUpdated events, so we
            // use removeListner to stop getting called when onUpdated events fire.
            chrome.tabs.onUpdated.removeListener(listener);

            // Look through all views to find the window which will display
            // the screenshot.  The url of the tab which will display the
            // screenshot includes a query parameter with a unique id, which
            // ensures that exactly one view will have the matching URL.
            var views = chrome.extension.getViews();
            for (var i = 0; i < views.length; i++) {
                var view = views[i];
                if (view.location.href == viewTabUrl) {
                    // Must be declared at web_accessible_resources in manifest.json


                    var pdf = new jsPDF('p', 'pt', 'letter');
                    var canvas = pdf.canvas;
                    view.setPdf(pdf);

                    canvas.width = 8.5 * 72;

                    html2canvas(document.body, {
                        canvas: canvas,
                        onrendered: function (canvas) {
                            view.renderPdf();

                            //var div = document.createElement('pre');
                            //div.innerText=pdf.output();
                            //document.body.appendChild(div);
                        }
                    });
                    view.setScreenshotUrl(screenshotUrl);
                    break;
                }
            }
        });

        chrome.tabs.create({url: viewTabUrl}, function (tab) {
            targetId = tab.id;
        });


        function createPDF() {
            var views = chrome.extension.getViews();

//    iframe.src = chrome.runtime.getURL('frame.html');
//
//    // Some styles for a fancy sidebar
//    iframe.style.cssText = 'position:fixed;top:0;left:0;display:block;' +
//            'width:300px;height:100%;z-index:1000;';
        }
        ;
    });
});
